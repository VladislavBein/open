# -*- coding: utf8 -*-
import requests
import json


class TokenForMySklad:
	"""Объект - Токен для МойСклад"""

	__LOGIN = ""
	__PASSWORD = ""

	def get_token(self) -> dict:
		"""Получает токен авторизации"""
		return self._parsing_token()

	def _parsing_token(self) -> dict:
		"""Парсит токен"""
		url = "https://online.moysklad.ru/api/remap/1.2/security/token"
		headers = {"Content-type": "application/json"}
		request = requests.post(url, headers=headers, auth=(TokenForMySklad.__LOGIN, TokenForMySklad.__PASSWORD))
		response = json.loads(request.content)
		token = {    
			"Authorization": response["access_token"],
			"Content-type": "application/json"    
		}
		return token


class ProcessingOrderFromMySklad:
	"""Объект - Заказ на производство из МойСклад"""

	def __init__(self, token: dict, url_processing_order: str):
		self.__token = token
		self.__url_processing_order = url_processing_order

	def get_processing_order(self) -> dict:
		"""Получает заказ на производство"""
		return self._parsing_processing_order()

	def _parsing_processing_order(self) -> dict:
		"""Парсит заказ на производство"""
		request = requests.get(self.__url_processing_order, headers=self.__token)
		response = json.loads(request.content)
		processing_order = response
		return processing_order 


class TechOperationsFromMySklad:
	"""Объект - Технологические операции из МойСклад"""

	def __init__(self, token: dict, processing_order_ms: object):
		self.__processing_order = processing_order_ms.get_processing_order()
		self.__token = token

	def get_tech_operations(self) -> list:
		"""Получает тех. операции"""
		return self._parsing_tech_operations()

	def _parsing_tech_operations(self) -> list:
		"""Парсит тех. операции"""
		list_tech_operations = []
		processings = self.__processing_order["processings"]
		for process in processings:
			url_tech_operation = process["meta"]["href"]
			request = requests.get(url_tech_operation, headers=self.__token)
			response = json.loads(request.content)
			tech_operation = response
			list_tech_operations.append((
				tech_operation["name"], tech_operation["moment"], tech_operation["quantity"]
			))
		return list_tech_operations


class CustomerOrderFromMySklad:
	"""Объект - Заказ покупателя из МойСклад"""

	def __init__(self, token: dict, url_customer_order: str):
		self.__token = token
		self.__url_customer_order = url_customer_order

	def get_customer_order(self) -> dict:
		"""Получает заказ покупателя"""
		return self._parsing_customer_order()

	def _parsing_customer_order(self) -> dict:
		"""Парсит заказ покупателя"""
		request = requests.get(self.__url_customer_order, headers=self.__token)
		response = json.loads(request.content)
		customer_order = response
		return customer_order


class ShipmentsFromMySklad:
	"""Объект - Отгрузки из МойСклад"""

	def __init__(self, customer_order_ms: object, token: dict):
		self.__customer_order = customer_order_ms.get_customer_order()
		self.__token = token

	def get_shipments(self) -> list:
		"""Получает отгрузки"""
		return self._parsing_shipments()

	def _parsing_shipments(self) -> list:
		"""Парсит отгрузки"""
		list_shipments = []
		demands = self.__customer_order["demands"]
		for demand in demands:
			url_shipment = demand["meta"]["href"]
			request = requests.get(url_shipment, headers=self.__token)
			response = json.loads(request.content)
			shipment = response
			list_shipments.append((
				shipment["name"], shipment["moment"], shipment["quantity"]
			))
		return list_shipments


class DataForAnalysTimeStock:
	"""Объект - Данные для анализа времени хранения"""

	__INFO_FOR_ANALYS = {
		"ТЛ": {
			"url_processing_order": None,
			"url_customer_order": None
		},
		"ЧМУ":{
			"url_processing_order": None,
			"url_customer_order": None
		}
	}

	def __init__(self):
		self.__token = TokenForMySklad().get_token()

	def get_data(self) -> tuple:
		"""Получает данные"""
		return self._get_data(company_name="ТЛ"), self._get_data(company_name="ЧМУ")

	def _get_data(self, company_name: str) -> tuple:
		"""Получает данные соответствующей компании"""
		processing_order_ms = ProcessingOrderFromMySklad(
			self.__token,
			url_processing_order=__INFO_FOR_ANALYS[company_name]["url_processing_order"]
		)
		tech_operations = TechOperationsFromMySklad(self.__token, processing_order_ms).get_tech_operations()
		customer_order_ms = CustomerOrderFromMySklad(
			self.__token,
			url_customer_order=__INFO_FOR_ANALYS[company_name]["url_customer_order"]
		)
		shipments = ShipmentsFromMySklad(self.__token, customer_order_ms)
		return tech_operations, shipments


class AnalysTimeStock:
	"""Объект - Анализ времени хранения"""
	pass
